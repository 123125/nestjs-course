export const frontendDomain = 'someFrontendUrl.com';

export enum FrontendPath {
    RESET_PASSWORD = '/auth/reset-password',
    CONFIRM_ACCOUNT = '/auth/confirm-account',
}

export enum TemplatePath {
    FORGOT_PASSWORD = './forgot-password',
    CONFIRM_ACCOUNT = './confirm-account',
}