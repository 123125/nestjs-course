import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { User } from '../auth/entities/user.entity';
import { frontendDomain, FrontendPath, TemplatePath } from './mail.models';

@Injectable()
export class MailService {

    constructor(private mailerService: MailerService) {}

    async sendUserForgotPassword(user: User, token: string) {
        const url = `${frontendDomain}${FrontendPath.RESET_PASSWORD}?token=${token}`;
        const { email, firstName, lastName } = user;

        await this.mailerService.sendMail({
            to: email,
            template: TemplatePath.FORGOT_PASSWORD,
            context: {
                name: firstName + ' ' + lastName,
                url
            },
        });
    }

    async sendUserConfirmAccount(user: User, token: string) {
        const url = `${frontendDomain}${FrontendPath.CONFIRM_ACCOUNT}?token=${token}`;
        const { email, firstName, lastName } = user;

        await this.mailerService.sendMail({
            to: email,
            template: TemplatePath.CONFIRM_ACCOUNT,
            context: {
                name: firstName + ' ' + lastName,
                url
            },
        });
    }
}
