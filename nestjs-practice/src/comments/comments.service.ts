import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../auth/entities/user.entity';
import { Repository } from 'typeorm';
import { Comment } from './entities/comment.entity';
import { Article } from '../articles/entities/article.entity';
import { CreateCommentDto } from './dto/create-comment.dto';
import { GetCommentsFilterDto } from './dto/get-comments-filter.dto';

@Injectable()
export class CommentsService {

    constructor(
        @InjectRepository(Comment) private commentRepository: Repository<Comment>,
        @InjectRepository(Article) private articleRepository: Repository<Article>
    ) {}

    async createComment(dto: CreateCommentDto, user: User) {
        const { articleId, commentId, text } = dto;
        const article = await this.articleRepository.findOne(articleId);

        if (!article) {
            throw new NotFoundException(`Article with id ${articleId} isn't existed`);
        }

        try {
            const comment = await this.commentRepository.create({
                commentId,
                article,
                user,
                text
            });
            return comment;
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async editComment(id: number, text: string, user: User) {
        const comment = await this.commentRepository.findOne({ id, userId: user.id });
        
        if (!comment) {
            throw new NotFoundException(`Comment with id ${id} isn't found`);
        }
        
        try {
            comment.text = text;
            comment.save();
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async getCommentsByArticle(articleId: number, dto: GetCommentsFilterDto) {

    }

    async deleteComment(id: number, user: User) {
        const result = await this.commentRepository.delete({id, userId: user.id});

        if (result.affected === 0) {
            throw new NotFoundException(`Comment with id ${id} is not found`)
        }
    }
}
