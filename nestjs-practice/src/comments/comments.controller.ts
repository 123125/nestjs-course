import { Body, Controller, Delete, Get, Param, Patch, Post, Query, ValidationPipe } from '@nestjs/common';
import { userInfo } from 'os';
import { User } from 'src/auth/entities/user.entity';
import { GetUser } from 'src/decorators/get-user.decorator';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { GetCommentsFilterDto } from './dto/get-comments-filter.dto';

@Controller('comments')
export class CommentsController {
    constructor(private commentsService: CommentsService) {}

    @Post()
    createComment(
        @Body(new ValidationPipe({ transform: true })) dto: CreateCommentDto,
        @GetUser() user: User
    ) {
        return this.commentsService.createComment(dto, user);
    }

    @Patch('/:id')
    editComment(
        @Param('id') id: number,
        @Body('text') text: string,
        @GetUser() user: User
    ) {
        return this.commentsService.editComment(id, text, user);
    }

    @Get('/:articleId')
    getCommentsByArticle(
        @Param('articleId') articleId: number,
        @Query() dto: GetCommentsFilterDto
    ) {
        return this.commentsService.getCommentsByArticle(articleId, dto);
    }

    @Delete('/:id')
    deleteComment(
        @Param() id: number,
        @GetUser() user: User,
    ) {
        return this.commentsService.deleteComment(id, user);
    }
}
