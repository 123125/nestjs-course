import { IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateCommentDto {
    @IsNumber()
    articleId: number;

    @IsOptional()
    @IsNumber()
    commentId: number;

    @IsString()
    text: string;

}