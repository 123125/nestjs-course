import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../../auth/entities/user.entity';
import { Article } from '../../articles/entities/article.entity';

@Entity()
export class Comment extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @JoinColumn({name: 'userId'})
    user: User;

    @Column()
    userId: number;

    @ManyToOne(type => Article, article => article.comments)
    @JoinColumn({name: 'articleId'})
    article: Article;

    @Column()
    commentId: number = null;

    @Column()
    text: string;

    @Column()
    createdAt: string = new Date().toISOString();

    @Column()
    updatedAt: string = new Date().toISOString();
}