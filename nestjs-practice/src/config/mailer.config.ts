import * as config from 'config';
import { join } from 'path';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';

const mailConfig = config.get('mail');
export const mailerConfig = {
    transport: {
        host: mailConfig.host,
        port: mailConfig.port,
        secure: true,
        requireTLS: true,
        auth: {
            user: mailConfig.user,
            pass: mailConfig.password,
        },
    },
    defaults: {
        from: `"No Reply" ${mailConfig.from}`,
    },
    template: {
        dir: join(__dirname, '/../mail/templates'),
        adapter: new HandlebarsAdapter(),
        options: {
            strict: true,
        },
    },
}