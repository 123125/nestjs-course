import * as config from 'config';
import { diskStorage } from 'multer';
import { filesDirectory } from '../files/models/files.constants';
import { editFileName } from '../files/filters/edit-file-name.filter';
import { imageFileFilter } from '../files/filters/image-file.filter';

const { maxFileSize } = config.get('multer');

// Multer upload options
export const multerOptions = {
    limit: {
        fileSize: maxFileSize
    },
    fileFilter: imageFileFilter,
    storage: diskStorage({
        destination: filesDirectory,
        filename: editFileName
    })

}