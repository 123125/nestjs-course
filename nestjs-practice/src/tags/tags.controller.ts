import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards, ValidationPipe } from '@nestjs/common';
import { User } from '../auth/entities/user.entity';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { GetUser } from '../decorators/get-user.decorator';
import { Tag } from './entities/tag.entity';
import { TagsService } from './tags.service';

@Controller('tags')
@UseGuards(JwtAuthGuard)
export class TagsController {

    constructor(private tagsService: TagsService) {}

    @Get()
    getAllTags(): Promise<Array<Tag>> {
        return this.tagsService.getAllTags();
    }

    @Post('/:name')
    createTag(
        @Param('name') name: string,
        @GetUser() user: User
    ): Promise<Tag> {
        return this.tagsService.createTag(name, user);
    }

    @Patch('/:id/:name') 
    editTag(
        @Param('id') id: number,
        @Param('name') name: string,
        @GetUser() user: User
    ): Promise<Tag> {
        return this.tagsService.editTag(id, name, user);
    }

    @Delete('/:id')
    deleteTag(
        @Param('id') id: number,
        @GetUser() user: User
    ): Promise<void> {
        return this.tagsService.deleteTag(id, user);
    }
}
