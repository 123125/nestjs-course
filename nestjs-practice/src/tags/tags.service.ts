import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../auth/entities/user.entity';
import { Tag } from './entities/tag.entity';

@Injectable()
export class TagsService {

    constructor(
        @InjectRepository(Tag) private tagRepository: Repository<Tag>
    ) {}

    async getAllTags(): Promise<Array<Tag>> {
        const tags = await this.tagRepository.find();
        return tags;
    }

    async createTag(name: string, user: User ): Promise<Tag> {
        try {
            const tag = await this.tagRepository.create({name});
            await tag.save();
            return tag;
        } catch (e) {
            // TODO: add logger
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async editTag( id: number, name: string, user: User ): Promise<Tag> {
        const tag = await this.tagRepository.findOne(id);

        if (!tag) {
            throw new NotFoundException();
        }

        tag.name = name;

        try {
            await tag.save();
            return tag;
        } catch (e) {
            // TODO: add logger
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async deleteTag( id: number, user: User): Promise<void> {
        const result = await this.tagRepository.delete(id);

        if (result.affected === 0) {
            throw new NotFoundException(`Task with id ${id} is not found`)
        }
    }
}
