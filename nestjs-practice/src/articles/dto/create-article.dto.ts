import { IsString,  IsArray, IsBoolean, MinLength, IsNotEmpty, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';
import { Tag } from '../../tags/entities/tag.entity';

export class CreateArticleDto {
    @IsString()
    @MinLength(5)
    @IsNotEmpty()
    title: string;

    @IsString()
    @MinLength(10)
    @IsNotEmpty()
    text: string;

    @IsArray()
    @IsOptional()
    tags: Array<Tag> = [];

    @IsString()
    @IsOptional()
    image: string;

    @Transform(it => {
        switch (it.toString()) {
          case 'true':
            return true;
          case 'false':
            return false;
          default:
            return it;
        }
      }, { toClassOnly: true }
    )
    @IsBoolean()
    @IsOptional()
    isPublished: boolean = false;
}