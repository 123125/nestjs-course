import { IsArray, IsNotEmpty } from 'class-validator';

export class AddTagsToArticleDto {
    @IsArray()
    @IsNotEmpty()
    tags: Array<number>;
}