import { IsString,  IsArray, IsBoolean, MinLength, IsNotEmpty, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';
import { Tag } from '../../tags/entities/tag.entity';

export class EditArticleDto {
    @IsString()
    @MinLength(5)
    @IsOptional()
    title: string;

    @IsString()
    @MinLength(10)
    @IsOptional()
    text: string;

    @IsArray()
    @IsOptional()
    tags: Array<Tag> = [];

    @IsString()
    @IsOptional()
    image: string;

    @Transform(it => {
        switch (it.toString()) {
          case 'true':
            return true;
          case 'false':
            return false;
          default:
            return it;
        }
      }, { toClassOnly: true }
    )
    @IsBoolean()
    @IsOptional()
    isPublished: boolean = false;
}