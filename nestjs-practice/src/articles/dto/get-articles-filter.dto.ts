import { IsNumber, IsString, IsOptional, IsArray, IsEnum, IsIn, ValidateNested } from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { SortArticleField } from '../models/sort-article-fields.enum';
import { Tag } from 'src/tags/entities/tag.entity';

export class GetArticlesFilterDto {
    @IsNumber()
    @IsOptional()
    @Transform(({value}) => Number(value), { toClassOnly: true })
    page: number = 1;

    @IsNumber()
    @IsOptional()
    @Transform(({value}) => Number(value), { toClassOnly: true })
    take: number = 10;

    @IsString()
    @IsOptional()
    @IsEnum(
        SortArticleField, 
        { message: `sortField must be one of the following values - ${Object.values(SortArticleField)}` 
    })
    sortField: SortArticleField = SortArticleField.CREATED_AT;

    @IsString()
    @IsOptional()
    @Transform(({ value }) => value.toUpperCase(), { toClassOnly: true })
    @IsIn(['ASC', 'DESC'])
    sortDirection: 'ASC' | 'DESC' = 'DESC';

    @IsOptional()
    @Transform(({ value }) => value.split(','), { toClassOnly: true })
    @IsArray()
    tagFilter: Tag[];

    @IsString()
    @IsOptional()
    authorIdFilter: number;

    @IsString()
    @IsOptional()
    search: string;
}