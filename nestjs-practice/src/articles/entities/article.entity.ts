import { BaseEntity, Entity, PrimaryGeneratedColumn, ManyToOne, Column, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { User } from '../../auth/entities/user.entity';
import { Tag } from '../../tags/entities/tag.entity';
import { Comment } from '../../comments/entities/comment.entity';
import { ArticleImage } from '../../files/entities/article-image.entity';

@Entity()
export class Article extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    text: string;

    @Column()
    createdAt: string = new Date().toISOString();

    @Column()
    updatedAt: string = new Date().toISOString();

    @Column()
    isPublished: boolean = false;

    @Column()
    authorId: number;
    
    @ManyToOne(type => User, user => user.articles)
    author: User;

    @ManyToMany(type => Tag, tags => tags.articles, { cascade: true, eager: true })
    @Column('simple-array', { default: [] })
    @JoinTable()
    tags: Array<Tag>;

    @OneToMany(type => ArticleImage, image => image.article, { cascade: true, eager: true })
    @Column('simple-array', { array: true })
    @JoinTable()
    images: ArticleImage[];

    @OneToMany(type => Comment, comment => comment.article )
    comments: Comment[];
}
