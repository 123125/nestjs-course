import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UploadedFile, UploadedFiles, UseGuards, UseInterceptors, ValidationPipe } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { GetUser } from '../decorators/get-user.decorator';
import { User } from '../auth/entities/user.entity';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { GetArticlesFilterDto } from './dto/get-articles-filter.dto';
import { EditArticleDto } from './dto/edit-article.dto';
import { AddTagsToArticleDto } from './dto/add-tags-to-article.dto';
import { ArticlesResponse } from './models/articles-response.model';
import { Article } from './entities/article.entity';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { multerOptions } from '../config/multer.config';

@Controller('articles')
@UseGuards(JwtAuthGuard)
export class ArticlesController {

    constructor(
        private articlesService: ArticlesService
    ) {}

    @Get()
    getAllArticles(
        @Query(new ValidationPipe({ transform: true })) dto: GetArticlesFilterDto
    ): Promise<ArticlesResponse> {
        return this.articlesService.getAllArticles(dto);
    }

    @Get('/:id')
    getArticleById(@Param('id') id: number): Promise<Article> {
        return this.articlesService.getArticleById(id);
    }

    @Post()
    createArticle(
        @Body(new ValidationPipe({ transform: true })) dto: CreateArticleDto,
        @GetUser() user: User
    ): Promise<Article> {
        return this.articlesService.createArticle(dto, user);
    }

    @Patch('/:id') 
    editArticle(
        @Param('id') id: number,
        @Body(new ValidationPipe({ transform: true })) dto: EditArticleDto,
        @GetUser() user: User
    ): Promise<Article> {
        return this.articlesService.editArticle(id, dto, user);
    }

    @Patch('/:id/addTags') 
    addTagsToArticle(
        @Param('id') id: number,
        @Body(new ValidationPipe({ transform: true })) dto: AddTagsToArticleDto,
        @GetUser() user: User
    ): Promise<void> {
        return this.articlesService.addTagsToArticle(id, dto, user);
    }

    @Patch('/:id/removeTags') 
    removeTagsForArticle(
        @Param('id') id: number,
        @Body(new ValidationPipe({ transform: true })) dto: AddTagsToArticleDto,
        @GetUser() user: User
    ): Promise<void> {
        return this.articlesService.removeTagsForArticle(id, dto, user);
    }

    @Delete('/:id')
    deleteArticle(
        @Param('id') id: number,
        @GetUser() user: User
    ): Promise<void> {
        return this.articlesService.deleteArticle(id, user);
    }

    @Post('/:id/uploadImages')
    @UseInterceptors( FilesInterceptor("images", 20, multerOptions) )
    uploadImagesToArticle(
        @Param('id') id: number,
        @UploadedFiles() images: Express.Multer.File[],
        @GetUser() user: User
    ) {
        console.log(id, images);
        return this.articlesService.uploadImagesToArticle(id, images, user);
    }


}
