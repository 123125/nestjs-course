import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../auth/entities/user.entity';
import { Tag } from '../tags/entities/tag.entity';
import { Repository } from 'typeorm';
import { AddTagsToArticleDto } from './dto/add-tags-to-article.dto';
import { CreateArticleDto } from './dto/create-article.dto';
import { EditArticleDto } from './dto/edit-article.dto';
import { GetArticlesFilterDto } from './dto/get-articles-filter.dto';
import { Article } from './entities/article.entity';
import { SelectedArticleFields } from './models/article.constants';
import { ArticlesResponse } from './models/articles-response.model';
import { MFile } from '../files/models/m-file.model';
import { FilesService } from '../files/files.service';
import { readFileSync } from 'fs';

@Injectable()
export class ArticlesService {
    constructor(
        @InjectRepository(Article) private articleRepository: Repository<Article>,
        @InjectRepository(Tag) private tagRepository: Repository<Tag>,
        private filesService: FilesService
    ) {}

    async getAllArticles(dto: GetArticlesFilterDto): Promise<ArticlesResponse> {
        const { page, take, sortField, sortDirection, tagFilter, authorIdFilter, search } = dto;
        const skip = (page - 1) * take;
        const query = this.articleRepository.createQueryBuilder('article');
        
        query.leftJoinAndSelect('article.author', 'author');
        query.leftJoinAndSelect('article.tags', 'tags');
        query.leftJoinAndSelect('article.images', 'images');
        
        if (authorIdFilter) {
            query.where({authorId: authorIdFilter});
        }

        if (tagFilter && tagFilter.length) {
            query.andWhere("tags.name IN (:...tagFilter)", { tagFilter })
        }

        if (search) {
            query.andWhere(
                `LOWER(article.title) LIKE LOWER(:search) 
                 OR LOWER(author.firstName) LIKE LOWER(:search)
                 OR LOWER(author.lastName) LIKE LOWER(:search)`,
                { search: `%${search}%` } );
        }
        
        query.select(SelectedArticleFields).skip(skip).take(take);

        query.orderBy(`article.${sortField}`, sortDirection);

        try {
            const result = await query.getManyAndCount();
            return { items: result[0], count: result[1] }
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async getArticleById(id: number): Promise<Article> {
        const query = await this.articleRepository.createQueryBuilder('article');

        query.leftJoinAndSelect('article.author', 'author');
        query.leftJoinAndSelect('article.tags', 'tags');
        query.leftJoinAndSelect('article.images', 'images');

        query.where({id});
        query.select([
            ...SelectedArticleFields,
            'article.text'
        ]);
        const article = await query.getOne();

        if (!article) {
            throw new NotFoundException();
        }

        return article;
    }

    async createArticle(dto: CreateArticleDto, user: User): Promise<Article> {
        const article = await this.articleRepository.create({
            ...dto
        });

        article.author = user;

        try {
            await article.save();
            return article;
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async editArticle(id: number, dto: EditArticleDto, user: User): Promise<Article> {
        const article = await this.articleRepository.findOne({ id, authorId: user.id });

        if (!article) {
            throw new NotFoundException();
        }

        const { title, text, tags, image, isPublished } = dto;

        if (title) article.title = title;
        if (text) article.text = text;
        // if (tags && tags.length) article.tags = tags;
        // if (image) article.image = image;
        if (typeof isPublished === 'boolean') article.isPublished = isPublished;

        try {
            await article.save();
            return article;
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async deleteArticle(id: number, user: User): Promise<void> {
        const result = await this.articleRepository.delete({id, authorId: user.id});

        if (result.affected === 0) {
            throw new NotFoundException(`Article with id ${id} is not found`)
        }
    }

    async addTagsToArticle(id: number, dto: AddTagsToArticleDto, user: User): Promise<void> {
        const article = await this.articleRepository.findOne({ id, authorId: user.id });
        if (!article) {
            throw new NotFoundException();
        }

        const { tags } = dto;
        await Promise.all(
            tags.map(async (tagId) => {
                const tag = await this.tagRepository.findOne(tagId);
                if (!tag) {
                    throw new NotFoundException(`tag with id ${tagId} isn\'t found`);
                }
                article.tags.push(tag);
            })
        );

        try {
            await article.save();
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async removeTagsForArticle(id: number, dto: AddTagsToArticleDto, user: User): Promise<void> {
        const article = await this.articleRepository.findOne({ id, authorId: user.id });
        if (!article) {
            throw new NotFoundException();
        }

        const { tags } = dto;
        tags.map(async (tagId) => {
            const tag = article.tags.find(tag => tag.id == tagId);
            if (!tag) {
                throw new NotFoundException(`tag with id ${tagId} isn\'t found`);
            }
            article.tags = article.tags.filter(tag => tag.id != tagId);
        });

        try {
            await article.save();
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async uploadImagesToArticle(id: number, images: Express.Multer.File[], user: User) {
        const article = await this.articleRepository.findOne({ id, authorId: user.id });
        if (!article) {
            throw new NotFoundException();
        }

        const saveImages: MFile[] = images.map(image => new MFile({
            originalname: image.originalname,
            buffer: readFileSync(image.path)
        }));

        try {
            const imagesResult = await this.filesService.saveFiles(saveImages);
            console.log(11111, imagesResult)
            article.images = imagesResult;
            await article.save();
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }
}
