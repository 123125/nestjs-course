import { Article } from '../entities/article.entity';

export class ArticlesResponse {
    items: Article[];
    count: number;
}