export enum SortArticleField {
    CREATED_AT = 'createdAt',
    ID = 'id',
    TITLE = 'title',
    TEXT = 'text',
    UPDATED_AT = 'updatedAt',
    IS_PUBLISHED = 'isPublished',
}