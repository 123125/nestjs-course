import { Body, Controller, Post, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ChangePasswordDto } from './dto/change-password.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { ResponseJwtToken } from './models/response-jwt-token.model';
import { ResponseMessage } from './models/response-message.model';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @Post('/registration')
    register( @Body(ValidationPipe) dto: CreateUserDto): Promise<ResponseMessage> {
        return this.authService.register(dto);
    }

    @Post('/confirm-registration')
    confirmRegister( @Query('email', ValidationPipe) email: string): Promise<ResponseJwtToken> {
        return this.authService.confirmRegister(email);
    }

    @Post('/resend-letter')
    sendConfirmRegisterLetter( @Query('email', ValidationPipe) email: string): Promise<ResponseMessage> {
        return this.authService.sendConfirmRegisterLetter(null, email);
    }

    @Post('/login')
    login( @Body(ValidationPipe) dto: LoginUserDto): Promise<ResponseJwtToken> {
        return this.authService.login(dto);
    }

    @Post('/forgot-password')
    forgotPassword(@Query('email', ValidationPipe) email: string): Promise<ResponseMessage> {
        return this.authService.forgotPassword(email);
    }

    @Post('/reset-password')
    resetPassword(@Body(ValidationPipe) dto: ResetPasswordDto): Promise<ResponseJwtToken> {
        return this.authService.resetPassword(dto);
    }

    @Post('/change-password')
    @UseGuards(JwtAuthGuard)
    changePassword(@Body(ValidationPipe) dto: ChangePasswordDto): Promise<ResponseJwtToken> {
        return this.authService.changePassword(dto);
    }
}
