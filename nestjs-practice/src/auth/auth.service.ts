import { BadRequestException, ConflictException, Injectable, InternalServerErrorException, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { User } from './entities/user.entity';
import { MailService } from 'src/mail/mail.service';
import { ChangePasswordDto } from './dto/change-password.dto';
import { ResponseMessage } from './models/response-message.model';
import { ResponseJwtToken } from './models/response-jwt-token.model';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User) private userRepository: Repository<User>,
        private jwtService: JwtService,
        private mailService: MailService
    ) {}

    async register(dto: CreateUserDto): Promise<ResponseMessage> {
        const { password } = dto;

        const salt = await bcrypt.genSalt();
        const hashedPassword = await bcrypt.hash(password, salt);

        const user = this.userRepository.create({
            ...dto,
            isConfirmed: false,
            password: hashedPassword,
            salt,
        });

        try {
            await user.save();
            return this.sendConfirmRegisterLetter(user);
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            if (e.code === ErrorCode.DUPLICATE_UNIQUE_FIELDS) {
                throw new ConflictException(ErrorMessage.EMAIL_LOGIN_NOT_UNIQUE);
            }
            throw new InternalServerErrorException();
        }
    }

    async sendConfirmRegisterLetter(user: User, email?: string): Promise<ResponseMessage> {
        if (!user && email) {
            user = await this.userRepository.findOne({ email });
        } 
        if (!user) {
            throw new NotFoundException();
        }
        const emailToken = Math.floor(1000 + Math.random() * 9000).toString();

        try {
            await this.mailService.sendUserConfirmAccount(user, emailToken);
            return {
                message: `Confirm letter was sent to the email ${email}`
            };
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async confirmRegister(email: string): Promise<ResponseJwtToken> {
        const user = await this.userRepository.findOne({ email });

        if (!user) {
            throw new NotFoundException();
        }
        
        try {
            user.isConfirmed = true;
            await user.save();
            return this.returnToken(user);
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async login(dto: LoginUserDto): Promise<ResponseJwtToken> {
        const user = await this.validateUser(dto);
        if (!user) {
            throw new UnauthorizedException();
        }

        if (!user.isConfirmed) {
            throw new UnauthorizedException({message: ErrorMessage.ACCOUNT_NOT_CONFIRMED});
        }

        return this.returnToken(user);
    }

    async forgotPassword(email: string): Promise<ResponseMessage> {
        const user = await this.userRepository.findOne({ email });
        if (!user) {
            throw new NotFoundException();
        }
        const emailToken = Math.floor(1000 + Math.random() * 9000).toString();

        try {
            await this.mailService.sendUserForgotPassword(user, emailToken);
            return {
                message: `Reset password letter was sent to the email ${email}`
            };
        } catch (e) {
            // TODO: add logger;
            console.log(e);
            throw new InternalServerErrorException();
        }
    }

    async resetPassword(dto: ResetPasswordDto): Promise<ResponseJwtToken> {
        const { email, confirmPassword, newPassword } = dto; 
        const user = await this.userRepository.findOne({ email });
        if (!user) {
            throw new NotFoundException();
        }
        if (newPassword !== confirmPassword) {
            throw new BadRequestException({message: ErrorMessage.NOT_MATCH_NEW_CONFIRM_PASSWORD});
        }
        try {
            user.salt = await bcrypt.genSalt();
            user.password = await bcrypt.hash(newPassword, user.salt);
            await user.save();
            return this.returnToken(user);
        } catch (e) {
            console.log(e);
            // TODO: add logger;
            throw new InternalServerErrorException();
        }
    }

    async changePassword(dto: ChangePasswordDto): Promise<ResponseJwtToken> {
        const { email, oldPassword, confirmPassword, newPassword } = dto; 
        const user = await this.userRepository.findOne({ email });

        if (!user) {
            throw new NotFoundException();
        }
        if (newPassword !== confirmPassword) {
            throw new BadRequestException({message: ErrorMessage.NOT_MATCH_NEW_CONFIRM_PASSWORD});
        }
        if ( !await this.validatePassword(oldPassword, user) ) {
            throw new BadRequestException({message: ErrorMessage.WRONG_OLD_PASSWORD});
        }

        try {
            user.salt = await bcrypt.genSalt();
            user.password = await bcrypt.hash(newPassword, user.salt);
            await user.save();
            return this.returnToken(user);
        } catch (e) {
            console.log(e);
            // TODO: add logger;
            throw new InternalServerErrorException();
        }
    }

    private async validateUser(dto: LoginUserDto): Promise<User> {
        const { login, password } = dto;
        const user = await this.userRepository.findOne({login})
        if (user && await this.validatePassword(password, user)) {
            return user;
        }
        return null; 
    }

    private async validatePassword(dtoPassword: string, user: User): Promise<boolean> {
        const { password, salt } = user;
        const hash = await bcrypt.hash(dtoPassword, salt);
        return hash === password;
    }

    private async returnToken(user: User): Promise<ResponseJwtToken> {
        const { firstName, lastName, email, phone, login, id } = user;
        const payload = { firstName, lastName, email, phone, login, id };
        const accessToken = await this.jwtService.sign(payload);
        return { accessToken};
    }
}
