const enum ErrorMessage {
    WEAK_PASSWORD = 'Password too weak',
    WRONG_OLD_PASSWORD = 'Old Password is wrong',
    NOT_MATCH_NEW_CONFIRM_PASSWORD ='New Password and Confirm Password aren\'t matched',
    ACCOUNT_NOT_CONFIRMED = 'The account isn\'t confirmed. Please check your email or resend the confirmation',
    EMAIL_LOGIN_NOT_UNIQUE = 'Login and Email have already used. These fields should be unique.',
}