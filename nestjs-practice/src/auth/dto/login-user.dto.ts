import { IsString, IsNotEmpty, Length, Matches } from 'class-validator';
import { passwordRegExp } from '../models/auth-constants';

export class LoginUserDto {
    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    login: string;

    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    @Matches( passwordRegExp, { message:  ErrorMessage.WEAK_PASSWORD })
    password: string;
}