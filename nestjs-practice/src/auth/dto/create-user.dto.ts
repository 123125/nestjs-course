import { IsString, IsEmail, IsNotEmpty, Length, IsPhoneNumber, Matches } from 'class-validator';
import { passwordRegExp } from '../models/auth-constants';

export class CreateUserDto {
    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    login: string;

    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    @Matches( passwordRegExp, { message:  ErrorMessage.WEAK_PASSWORD })
    password: string;

    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    firstName: string;

    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    lastName: string;

    @IsString()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsString()
    @IsNotEmpty()
    @IsPhoneNumber()
    phone: string;
}