import { IsString, IsNotEmpty, Length, Matches } from 'class-validator';
import { passwordRegExp } from '../models/auth-constants';

export class ResetPasswordDto {
    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    email: string;

    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    @Matches( passwordRegExp, { message:  ErrorMessage.WEAK_PASSWORD })
    newPassword: string;

    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    @Matches( passwordRegExp, { message:  ErrorMessage.WEAK_PASSWORD })
    confirmPassword: string;
}