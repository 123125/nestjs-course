import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { format } from 'date-fns';
import { ensureDir, writeFile } from 'fs-extra';
import { Repository } from 'typeorm';
import { ArticleImage } from './entities/article-image.entity';
import { FileResponse } from './models/file-response.model';
import { filesDirectory } from './models/files.constants';
import { MFile } from './models/m-file.model';

@Injectable()
export class FilesService {

    constructor(
        @InjectRepository(ArticleImage) private imageRepository: Repository<ArticleImage>,
    ) {}

    async saveFiles(files: MFile[]): Promise<ArticleImage[]> {
        const dateFolder = format(new Date, 'yyyy-MM-dd');
        const uploadFolder = `${filesDirectory}/${dateFolder}`;
        await ensureDir(uploadFolder);
        const res: FileResponse[] = [];
        for (const file of files) {
            await writeFile(`${uploadFolder}/${file.originalname}`, file.buffer );
            res.push({ url: `${uploadFolder}/${file.originalname}`, name: file.originalname });
        }
        const articleImages = await Promise.all(res.map(async image => {
            const articleImage = await this.imageRepository.create({...image});
            await articleImage.save();
            return articleImage;
        }));
        return articleImages;
    }
}
