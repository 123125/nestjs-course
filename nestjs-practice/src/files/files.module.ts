import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express/multer/multer.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import { filesDirectory } from './models/files.constants';
import { FilesService } from './files.service';
import { ArticleImage } from './entities/article-image.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([ArticleImage]),
    MulterModule.register({
      dest: filesDirectory
    }),
    ServeStaticModule.forRoot({
      rootPath: filesDirectory,
    })
  ],
  providers: [FilesService],
  exports: [FilesService],
})
export class FilesModule {}
