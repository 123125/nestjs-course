export const ALREADY_REGISTERED_ERROR = 'User has already existed';
export const USER_NOT_FOUND = 'User has not found';
export const WRONG_PASSWORD_ERROR = 'Password is wrong';