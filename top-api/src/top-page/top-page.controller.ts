import { Body, Controller, Delete, Get, HttpCode, NotFoundException, Param, Patch, Post, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { HhService } from '../hh/hh.service';
import { JwtAuthGuard } from '../auth/guards/jwt.guards';
import { IdValidationPipe } from '../pipes/id-validation.pipe';
import { CreateTopPageDto } from './dto/create-top-page.dto';
import { FindTopPageDto } from './dto/find-top-page.dto';
import { NOT_FOUND_PAGE_ERROR } from './top-page.constants';
import { TopPageService } from './top-page.service';
import { Cron, CronExpression, SchedulerRegistry } from '@nestjs/schedule';

@Controller('top-page')
export class TopPageController {

    constructor(
        private topPageService: TopPageService,
        private hhService: HhService,
        private readonly schedulerRegistry: SchedulerRegistry
    ) {}

    @UseGuards(JwtAuthGuard)
    @Post('create')
    async create(@Body() dto: CreateTopPageDto) {
        return this.topPageService.create(dto);
    }

    @UseGuards(JwtAuthGuard)
    @Get(':id') 
    async get(@Param('id', IdValidationPipe) id: string) {
        const page = await this.topPageService.findById(id);
        if (!page) {
            throw new NotFoundException(NOT_FOUND_PAGE_ERROR);
        }
        return page;
    }

    @Get('byAlias/:id') 
    async getByAlias(@Param('alias', IdValidationPipe) alias: string) {
        const page = await this.topPageService.findByAlias(alias);
        if (!page) {
            throw new NotFoundException(NOT_FOUND_PAGE_ERROR);
        }
        return page;
    }

    @UseGuards(JwtAuthGuard)
    @Delete(':id') 
    async delete(@Param('id', IdValidationPipe) id: string) {
        const deleted = await this.topPageService.deleteById(id);
        if (!deleted) {
            throw new NotFoundException(NOT_FOUND_PAGE_ERROR);
        }
        return deleted;
    }

    @UseGuards(JwtAuthGuard)
    @Patch(':id') 
    async patch(@Param('id', IdValidationPipe) id: string, @Body() dto: CreateTopPageDto) {
        const updated = await this.topPageService.updateById(id, dto);
        if (!updated) {
            throw new NotFoundException(NOT_FOUND_PAGE_ERROR);
        }
        return updated;
    }

    @UsePipes(new ValidationPipe())
    @HttpCode(200)
    @Post('find')
    async find(@Body() dto: FindTopPageDto) {
        return this.topPageService.findByCategory(dto.firstCategory);
    }

    @Get('textSearch/:text')
    async textSearch(@Param('text') text: string) {
        return this.topPageService.findByText(text);
    }

    @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT, { name: 'test' })
    async test() {
        const job = this.schedulerRegistry.getCronJob('test');
        const data = await this.topPageService.findForHhUpdate(new Date());
        for (let page of data) {
            const hhData = await this.hhService.getData(page.category);
            page.hh = hhData;
            await this.sleep();
            await this.topPageService.updateById(page._id, page);
        }
    }

    sleep() {
        return new Promise<void>((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, 100)
        })
    }
}
