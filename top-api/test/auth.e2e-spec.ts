import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { AuthDto } from '../src/auth/dto/auth.dto';

const loginDto: AuthDto = {
	login: 'a@a.ru',
	password: '1'
};

describe('LoginController (e2e)', () => {
    let app: INestApplication;
    let access_token: string;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();
    
        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/auth/login (POST) - success', async (done) => {
        return request(app.getHttpServer())
            .post('/auth/login')
            .send(loginDto)
            .expect(200)
            .then(({ body }: request.Response) => {
                access_token = body.access_token;
                expect(access_token).toBeDefined();
                done();
            });
    });

    it('/auth/login (POST) - fail password', async (done) => {
        return request(app.getHttpServer())
            .post('/auth/login')
            .send({ ...loginDto, password: '2' })
            .expect(401, {
                statusCode: 401,
                message: "Password is wrong",
                error: "Unauthorized"
            })
    });

    it('/auth/login (POST) - fail login', async (done) => {
        return request(app.getHttpServer())
            .post('/auth/login')
            .send({ ...loginDto, login: 'a2@a.ru' })
            .expect(401, {
                statusCode: 401,
                message: "User has not found",
                error: "Unauthorized"
            })
    }); 
});