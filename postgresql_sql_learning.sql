Конспект по курсу "Практический курс для новичков по SQL и PostgreSQL"
link -> https://coursehunter.net/course/prakticheskiy-kurs-dlya-novichkov-po-sql-i-postgresql
=========================================================================


Введение
-------------------------------------------------------------------------

-- создать базу данных:
CREATE DATABASE testdb;

-- создать таблицу:
CREATE TABLE publisher
(
	publisher_id integer PRIMARY KEY,
	org_name varchar(128) NOT NULL,
	adress text NOT NULL
);

CREATE TABLE book
(
	book_id integer PRIMARY KEY,
	title text NOT NULL,
	isbn varchar(32) NOT NULL
);

-- удалить таблицу:
DROP TABLE book;
DROP TABLE  IF EXISTS book;

-- добавить данные:
INSERT INTO piblisher
VALUES
(1, 'someName', 'someCity'),
(2, 'someName', 'someCity'),
(3, 'someName', 'someCity'),
(4, 'someName', 'someCity');

-- извлечь все данные:
SELECT * FROM book;

-- добавить таблицу и привзять ее к другой (один ко многи):
ALTER TABLE book
ADD COLUMN fk_publisher_id;

ALTER TABLE book
ADD CONSTRAINT fk_book_publisher
FOREIGN KEY(fk_publisher_id) REFERENCES publisher(publisher_id);

-- привязка один ко многим или один к одному при создании таблицы:
fk_publisher_id integer REFERENCES publisher(publisher_id) NOT NULL 		// <-- колонка/атрибут

-- привязка многие ко многим при создании таблицы:
CREATE TABLE book_author
(
		book_id int REFERENCES book(book_id),
		author_id int REFERENCES author(author_id),

		CONSTRAINT book_author_pkey PRIMARY KEY (book_id, author_id) --compose key
);


Простые выборки:
-------------------------------------------------------------------------

-- полная выборка данных с таблицы (плохая практика):
SELECT * FROM products

-- выборка конкертных данных с таблицы:
SELECT product_id, product_name, unit_price FROM products

-- выборка уникальных занчений с таблицы -> DISTINCT:
SELECT DISTINCT city, country FROM employees

-- получить число строк/записей
SELECT DISTINCT COUNT(country) FROM employees


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Выбрать все данные из таблицы customers
SELECT * FROM customers;

-- 2. Выбрать все записи из таблицы customers, но только колонки "имя контакта" и "город"
SELECT contact_name, city FROM customers;

-- 3. Выбрать все записи из таблицы orders, но взять две колонки: идентификатор заказа и колонку, значение в которой мы рассчитываем как разницу между датой отгрузки и датой формирования заказа.
SELECT order_id, shipped_date - order_date FROM orders;

-- 4. Выбрать все уникальные города в которых "зарегестрированы" заказчики
SELECT DISTINCT city FROM customers;

-- 5. Выбрать все уникальные сочетания городов и стран в которых "зарегестрированы" заказчики
SELECT DISTINCT city, country FROM customers;

-- 6. Посчитать кол-во заказчиков
SELECT COUNT(*) FROM customers;

-- 7. Посчитать кол-во уникальных стран в которых "зарегестрированы" заказчики
SELECT COUNT(DISTINCT country) FROM customers;


Простые выборки (продолжение):
-------------------------------------------------------------------------

-- фильтрация данных с таблицы:
SELECT * FROM table WHERE condition;

-- комбинирование условий - AND, OR:
SELECT * FROM table WHERE condition1 OR (condition2 AND condition3);

-- получить данные с интервалом:
SELECT * FROM table BETWEEN value1 AND value2;

-- фильтрация данных по списку значений - IN, NOT IN:
SELECT * FROM table WHERE column IN (value1, value2, value3);
SELECT * FROM table WHERE column NOT IN (value1, value2, value3);

-- сортировка по атрибуту - ORDER BY ASC/DESC;
SELECT * FROM table ORDER BY column ASC;
SELECT * FROM table ORDER BY column DESC;
SELECT * FROM table ORDER BY column1 DESC, column2 DESC;

-- получить минимальное значение из таблицы:
SELECT MIN(column1) FROM table WHERE condition;

-- получить максимальное значение из таблицы:
SELECT MAX(column1) FROM table WHERE condition;

-- получить среднее значение из таблицы:
SELECT AVG(column1) FROM table WHERE condition;

-- получить сумму значений из таблицы:
SELECT SUM(column1) FROM table WHERE condition;


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Выбрать все заказы из стран France, Austria, Spain
SELECT * FROM orders WHERE ship_country IN ('France', 'Austria', 'Spain');

-- 2. Выбрать все заказы, отсортировать по required_date (по убыванию) и отсортировать по дате отгрузке (по возрастанию)
SELECT * FROM orders ORDER BY required_date DESC, shipped_date ASC;

-- 3. Выбрать минимальную цену товара среди тех продуктов, которых в продаже более 30 единиц.
SELECT MIN(unit_price) FROM products WHERE units_in_stock > 30;

-- 4. Выбрать максимальное кол-во единиц товара среди тех продуктов, цена которых более 30 у.е.
SELECT MAX(units_in_stock) FROM products WHERE unit_price > 30;

-- 5. Найти среднее значение дней уходящих на доставку с даты формирования заказа в USA
SELECT AVG(shipped_date - order_date) FROM orders WHERE ship_country='USA';

-- 6. Найти сумму, на которую имеется товаров (кол-во * цену) причём таких, которые планируется продавать и в будущем (см. на поле discontinued)
SELECT SUM(unit_price * units_in_stock) FROM products WHERE discontinued <> 1;


Простые выборки (продолжение):
-------------------------------------------------------------------------

-- Паттерн матчинг c LIKE:
% - placeholder
_ - 1 symbol
LIKE 'U%' - строки, начинающиеся на U
LIKE '%a' - строки, кончающиеся на a
LIKE '%John%' - строки, содержащие John
LIKE 'J%n' - строки, начинающиеся на J и кончающиеся на a
LIKE '_oh_' - строки, где 2, 3 символы - oh, а первый (1) и последний (4) любые


-- LIMIT:
SELECT column FROM table WHERE condition1 LIMIT 10;

-- IS NULL, IS NOT NULL:
SELECT column FROM table WHERE condition1 IS NULL;
SELECT column FROM table WHERE condition1 IS NOT NULL;

-- групировка значений по атрибуту - GROUP BY:
SELECT column1, SUM(*) FROM table WHERE condition1 GROUP BY column1 ORDER BY SUM(*) DESC;

-- посфильтр HAVING:
SELECT column1, SUM(column2) FROM table WHERE condition1 GROUP BY column1 HAVING SUM(column2)>500 ORDER BY SUM(column2) DESC;

-- объединение UNION/UNION All:
SELECT column1 FROM table1 UNION SELECT column1 FROM table2;

-- получение записей где совпадают значения атрибутов INTERSECT:
SELECT column1 FROM table1 INTERSECT SELECT column1 FROM table2;

-- исключение EXCEPT/ EXCEPT ALL:
SELECT column1 FROM table1 EXCEPT SELECT column1 FROM table2;


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Выбрать все записи заказов в которых наименование страны отгрузки начинается с 'U'
SELECT * FROM orders WHERE ship_country LIKE 'U%';

-- 2. Выбрать записи заказов (включить колонки идентификатора заказа, идентификатора заказчика, веса и страны отгузки),
-- которые должны быть отгружены в страны имя которых начинается с 'N', отсортировать по весу (по убыванию) и вывести только первые 10 записей.
SELECT order_id, customer_id, freight, ship_country FROM orders WHERE ship_country LIKE 'N%' ORDER BY freight DESC LIMIT 10;

-- 3. Выбрать записи работников (включить колонки имени, фамилии, телефона, региона) в которых регион неизвестен
SELECT first_name, last_name, home_phone, region FROM employees WHERE region IS NULL;

-- 4. Подсчитать кол-во заказчиков регион которых известен
SELECT COUNT(*) FROM customers WHERE region IS NOT NULL;

-- 5. Подсчитать кол-во поставщиков в каждой из стран и отсортировать результаты группировки по убыванию кол-ва
SELECT country, COUNT(*) FROM suppliers GROUP BY country ORDER BY COUNT(*) DESC;

-- 6. Подсчитать суммарный вес заказов (в которых известен регион) по странам, затем отфильтровать по суммарному весу
-- (вывести только те записи где суммарный вес больше 2750) и отсортировать по убыванию суммарного веса.
SELECT SUM(freight) FROM orders WHERE ship_region IS NOT NULL HAVING SUM(freight) > 2759 ORDER BY SUM(freight) DESC;

-- 7. Выбрать все уникальные страны заказчиков и поставщиков и отсортировать страны по возрастанию
SELECT country FROM customers UNION SELECT country FROM suppliers;

-- 8. Выбрать такие страны в которых "зарегистированы" одновременно и заказчики и поставщики и работники.
SELECT country FROM customers INTERSECT SELECT country FROM suppliers INTERSECT SELECT country FROM employees;

-- 9. Выбрать такие страны в которых "зарегистированы" одновременно заказчики и поставщики, но при этом в них не "зарегистрированы" работники.
SELECT country FROM customers INTERSECT SELECT country FROM suppliers EXCEPT SELECT country FROM employees;


Соединения
-------------------------------------------------------------------------

-- INNER JOIN - соединяет записи в которых есть значения, остальные отсекаются
SELECT product_name, suppliers.company_name, units_in_stock
FROM products
INNER JOIN suppliers ON products.supplier_id = suppliers.supplier_id
ORDER BY units_in_stock DESC;

-- LEFT JOIN - соединяет записи c правой таблицы к левой, если значений или записи в правой нет - выводиться NULL
-- RIGHT JOIN - тоже самое, только наоборот
-- FULL JOIN - соединяет все записи, если значений - выводиться NULL
SELECT company_name, product_name
FROM suppliers
LEFT JOIN products ON suppliers.supplier_id = products.supplier_id;

-- SELF JOIN - соединение значений с разных колонок одной таблицы
SELECT e.first_name || ' ' || e.last_name AS employees,
	   m.first_name || ' ' || m.last_name AS manager
FROM employees e
LEFT JOIN employee m ON m.employee_id = e.manager_id
ORDER BY manager;

-- USING === ON suppliers.supplier_id = products.supplier_id если колонки совпадают
SELECT company_name, product_name
FROM suppliers
INNER JOIN products USING(supplier_id);

-- NATURAL JOIN - соединени по всем столбцам, которые проименованны одинаково
-- (не рекомендуется использовать - не очивидно, как соединяются столбцы, сложно дебажить)
-- NATURAL JOIN suppliers === INNER JOIN suppliers ON products.supplier_id = suppliers.supplier_id
SELECT company_name, product_name
FROM suppliers
NATURAL JOIN products;

-- AS - нейминг колонки
SELECT COUNT(*) AS employees_count
FROM employees;


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Найти заказчиков и обслуживающих их заказы сотрудников таких, что и заказчики и сотрудники из города London, а доставка идёт компанией Speedy Express.
-- Вывести компанию заказчика и ФИО сотрудника.
SELECT customers.company_name, last_name || ' ' || first_name
FROM orders
INNER JOIN employees USING(employee_id)
INNER JOIN customers USING(customer_id)
INNER JOIN shippers ON orders.ship_via = shippers.shipper_id
WHERE employees.city='London' AND customers.city='London'
AND shippers.company_name === 'Speedy Express';

-- 2. Найти активные (см. поле discontinued) продукты из категории Beverages и Seafood, которых в продаже менее 20 единиц. Вывести наименование продуктов, кол-во единиц в продаже,
 -- имя контакта поставщика и его телефонный номер.
SELECT product_name, units_in_stock, contact_title, phone
FROM products
INNER JOIN categories USING(category_id)
INNER JOIN suppliers USING(supplier_id)
WHERE discontinued=0
AND category_name IN ('Beverages', 'Seafood') 
AND units_in_stock < 20;

-- 3. Найти заказчиков, не сделавших ни одного заказа. Вывести имя заказчика и order_id.
SELECT company_name, order_id FROM customers
LEFT JOIN orders USING(customer_id)
WHERE order_id IS NULL;

-- 4. Переписать предыдущий запрос, использовав симметричный вид джойна (подсказка: речь о LEFT и RIGHT).
SELECT company_name, order_id FROM orders
RIGHT JOIN customers USING(customer_id)
WHERE order_id IS NULL;


Повзапросы
-------------------------------------------------------------------------

-- пример подзапроса
SELECT company_name FROM suppliers
WHERE country IN (SELECT country FROM customers);

-- пример подзапроса c WHERE EXISTS
SELECT company_name, contact_name
FROM customers
WHERE EXISTS (SELECT customer_id FROM orders
			  WHERE customer_id = customers.customer_id
			  AND freight BETWEEN 58 AND 100);

-- пример подзапроса c WHERE NOT EXISTS
SELECT company_name, contact_name
FROM customers
WHERE NOT EXISTS (SELECT customer_id FROM orders
			  	  WHERE customer_id = customers.customer_id
			  	  AND order_date BETWEEN '1995-02-01' AND '1995-02-15');

-- пример подзапроса c ANY
SELECT DISTINCT company_name
FROM customers
WHERE customer_id = ANY(
	SELECT customer_id
	FROM orders
	JOIN order_details USING(order_id)
	WHERE quantity > 40
);

-- пример подзапроса c ANY
SELECT DISTINCT product_name, quantity
FROM products
JOIN order_details USING(product_id)
WHERE quantity > ALL( SELECT AVG(quantity)
					  FROM order_details
					  GROUP BY product_id)
ORDER BY quantity;


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Вывести продукты количество которых в продаже меньше самого малого среднего количества продуктов в деталях заказов (группировка по product_id).
-- Результирующая таблица должна иметь колонки product_name и units_in_stock.
SELECT product_name, units_in_stock
FROM products
WHERE units_in_stock < ALL(
	SELECT AVG(quantity)
	FROM order_details
	GROUP BY product_id
)
ORDER BY units_in_stock DESC;

-- 2. Напишите запрос, который выводит общую сумму фрахтов заказов для компаний-заказчиков для заказов, стоимость фрахта которых больше или равна средней величине стоимости 
-- фрахта всех заказов, а также дата отгрузки заказа должна находится во второй половине июля 1996 года. 
-- Результирующая таблица должна иметь колонки customer_id и freight_sum, строки которой должны быть отсортированы по сумме фрахтов заказов.
SELECT customer_id, SUM(freight) AS freight_sum FROM orders
INNER JOIN (SELECT customer_id, AVG(freight) AS freight_avg
			FROM orders
			GROUP BY customer_id) oa
USING(customer_id)
WHERE freight > freight_avg
AND shipped_date BETWEEN '1996-07-16' AND '1996-07-31'
GROUP BY customer_id
ORDER BY freight_sum;


-- 3. Напишите запрос, который выводит 3 заказа с наибольшей стоимостью, которые были созданы после 1 сентября 1997 года включительно и были доставлены в страны Южной Америки. 
-- Общая стоимость рассчитывается как сумма стоимости деталей заказа с учетом дисконта. Результирующая таблица должна иметь колонки customer_id, ship_country и order_price,
-- строки которой должны быть отсортированы по стоимости заказа в обратном порядке.
SELECT customer_id, ship_country, order_price
FROM orders
JOIN (SELECT order_id, SUM(unit_price * quantity - unit_price * quantity * discount) AS order_price
	  FROM order_details
	  GROUP By order_id) AS od
USING (order_id)
WHERE ship_country IN ('Argentina', 'Bolivia', 'Brazil', 'Chile', 'Colombia', 'Ecuador', 'Guyana', 'Paraguay',
						'Peru', 'Suriname', 'Uruguay', 'Venezuela')
AND order_date >= '1997-09-01'
ORDER BY order_price DESC
LIMIT 3;

-- 4. Вывести все товары (уникальные названия продуктов), которых заказано ровно 10 единиц (конечно же, это можно решить и без подзапроса).
SELECT product_name FROM products
WHERE product_id = ANY(
	SELECT product_id FROM order_details WHERE quantity = 10
);


DDL - Data Definition Language
-------------------------------------------------------------------------
CREATE TABLE table_name

ALTER TABLE table_name
	ADD COLUMN column_name data_type
	RENAME TO new_table_new
	RENAME old_column_name TO new_column_name
	ALTER COLUMN column_name SET DATA TYPE data_type

DROP TABLE table_name

TRUNCATE TABLE table_name 			-- <-- delete data of table

DROP COLUMN column_name


Домашнее задание (Практика):
-------------------------------------------------------------------------
-- 1. Создать таблицу teacher с полями teacher_id serial, first_name varchar, last_name varchar, birthday date, phone varchar, title varchar
CREATE TABLE teacher
(
	teacher_id serial,
	first_name varchar,
	last_name varchar,
	birthday date,
	phone varchar,
	title varchar
);

-- 2. Добавить в таблицу после создания колонку middle_name varchar
ALTER TABLE teacher
ADD COLUMN middle_name varchar;

-- 3. Удалить колонку middle_name
ALTER TABLE teacher
DROP COLUMN middle_name;

-- 4. Переименовать колонку birthday в birth_date
ALTER TABLE teacher
RENAME COLUMN birthday TO birth_date;

-- 5. Изменить тип данных колонки phone на varchar(32)
ALTER TABLE teacher
ALTER COLUMN phone SET DATA TYPE varchar(32);

-- 6. Создать таблицу exam с полями exam_id serial, exam_name varchar(256), exam_date date
CREATE TABLE exam
(
	exam_id serial,
	exam_name varchar(256),
	exam_date date
);

-- 7. Вставить три любых записи с автогенерацией идентификатора
INSERT INTO exam (exam_name, exam_date)
VALUES
('exam1', '1999-01-01'),
('exam2', '1999-01-02'),
('exam3', '1999-01-03');

-- 8. Посредством полной выборки убедиться, что данные были вставлены нормально и идентификаторы были сгенерированы с инкрементом
SELECT * FROM exam;

-- 9. Удалить все данные из таблицы со сбросом идентификатор в исходное состояние
TRUNCATE TABLE exam RESTART IDENTITY;


DDL - Data Definition Language (продолжение)
-------------------------------------------------------------------------

-- ограничения:
PRIMARY KEY
UNIQUE NOT NULL
FOREIGN KEY

-- блочные ограничения при создании таблицы
CONSTRAINT PK_book_book_id PRIMARY KEY(book_id),
CONSTRAINT PK_book_publisher FOREIGN KEY (publisher_id) REFERENCES publisher(publisher_id);

-- удалить ограничение
ALTER TABLE book
DROP CONSTRAINT PK_book_publisher;

-- ограничения с условими
ADD COLUMN price decimal
CONSTRAINT CHK_book_price 
CHECK (price >= 0);

-- ограничения с условими c дефолтным значеним
status char DEFAULT 'active';

ALTER TABLE customer
ADD COLUMN status SET DEFAULT 'active';

-- последовательность - навешиивается на поле для автомматической генерации занчений

-- создать последовательность:
CREATE SEQUENCE seq1;

-- сгененрировать следующее значение последовательности:
SELECT nextval('seq1');

-- получить текущее значение последовательности:
SELECT currvul('seq1');

-- получить последнее значение последовательности:
SELECT lastvul();

-- установить значение последовательности:
SELECT setval('seq1', 16, true/false);

-- запрещать пользователю менять значение id
book_id int GENERATED ALWAYS AS IDENTITY NOT NULL,

-- UPDATE table
UPDATE author
SET full_name = 'Elias', rating = 5
WHERE author_id = 1;

-- DELETE rows
DELETE FROM author
WHERE rating < 4.5;

-- RETURNING - get values
INSERT INTO book (title, isbn, fk_publisher_id)
VALUES ('title', 'isbn', 3)
RETURNING title;


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Создать таблицу exam с полями:
-- - идентификатора экзамена - автоинкрементируемый, уникальный, запрещает NULL;
-- - наименования экзамена
-- - даты экзамена
CREATE TABLE exam (
	exam_id int serial UNIQUE NOT NUll,
	exam_name char,
	exam_date date
);

-- 2. Удалить ограничение уникальности с поля идентификатора
ALTER TABLE exam
DROP CONSTRAINT exam_exam_id_key;

-- 3. Добавить ограничение первичного ключа на поле идентификатора
ALTER TABLE exam
ADD PRIMARY KEY(exam_id);

-- 4. Создать таблицу person с полями
-- - идентификатора личности (простой int, первичный ключ)
-- - имя
-- - фамилия
CREATE TABLE person (
	person_id int PRIMARY KEY NOT NULL,
	first_name char NOT NULL,
	last_name char NOT NULL
);

-- 5. Создать таблицу паспорта с полями:
-- - идентификатора паспорта (простой int, первичный ключ)
-- - серийный номер (простой int, запрещает NULL)
-- - регистрация
-- - ссылка на идентификатор личности (внешний ключ)
CREATE TABLE passport (
	passport_id int PRIMARY KEY,
	passport_number int NOT NULL,
	registration char,
	person_id int,
	
	CONSTRAINT PK_passport_person FOREIGN KEY (person_id) REFERENCES person(person_id)
);

-- 6. Добавить колонку веса в таблицу book (создавали ранее) с ограничением, проверяющим вес (больше 0 но меньше 100)
ALTER TABLE book
ADD COLUMN book_weight decimal
CONSTRAINT chk_book_weight
CHECK (book_weight > 0 AND book_weight < 100);

-- 7. Убедиться в том, что ограничение на вес работает (попробуйте вставить невалидное значение)
INSERT INTO book
VALUES (1, 'title1', 'isbn1', 111);

-- 8. Создать таблицу student с полями:
-- - идентификатора (автоинкремент)
-- - полное имя
-- - курс (по умолчанию 1)
CREATE TABLE student (
	student_id int GENERATED ALWAYS AS IDENTITY NOT NULL,
	full_name text,
	cource int DEFAULT 1
);

-- 9. Вставить запись в таблицу студентов и убедиться, что ограничение на вставку значения по умолчанию работает
INSERT INTO student (student_id, full_name)
VALUES(1, 'ALEX');

-- 10. Удалить ограничение "по умолчанию" из таблицы студентов
ALTER TABLE student
ALTER COLUMN cource
DROP DEFAULT

-- 11. Подключиться к БД northwind и добавить ограничение на поле unit_price таблицы products (цена должна быть больше 0)
ALTER TABLE products ADD CONSTRAINT CHK_products_unit_price CHECK (unit_price > 0);

-- 12. "Навесить" автоинкрементируемый счётчик на поле product_id таблицы products (БД northwind).
-- Счётчик должен начинаться с числа следующего за максимальным значением по этому столбцу.
CREATE SEQUENCE IF NOT EXISTS product_id_seq
START WITH 78 OWNED BY products.product_id;

ALTER TABlE products
ALTER COLUMN product_id SET DEFAULT nextval('product_id_seq');

-- 13. Произвести вставку в products (не вставляя идентификатор явно) и убедиться, что автоинкремент работает.
-- Вставку сделать так, чтобы в результате команды вернулось значение, сгенерированное в качестве идентификатора
INSERT INTO products (product_name, discontinued)
VALUES('test name', 0)
RETURNING *;


VIEWS
-------------------------------------------------------------------------

Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Создать представление, которое выводит следующие колонки:
-- order_date, required_date, shipped_date, ship_postal_code, company_name, contact_name, phone, last_name, first_name, title из таблиц orders, customers и employees.
-- Сделать select к созданному представлению, выведя все записи, где order_date больше 1го января 1997 года.
CREATE VIEW customers_employees AS
SELECT order_date, required_date, shipped_date, ship_postal_code, company_name, contact_name, phone, last_name, first_name, title
FROM orders
JOIN employees USING(employee_id)
JOIN customers USING(customer_id);

SELECT * FROM customers_employees
WHERE order_date > '1997-01-01';

-- 2. Создать представление, которое выводит следующие колонки:
-- order_date, required_date, shipped_date, ship_postal_code, ship_country, company_name, contact_name, phone, last_name, first_name, title из таблиц orders, customers, employees.
-- Попробовать добавить к представлению (после его создания) колонки ship_country, postal_code и reports_to. Убедиться, что проихсодит ошибка. Переименовать представление и создать новое уже с дополнительными колонками.
-- Сделать к нему запрос, выбрав все записи, отсортировав их по ship_county.
-- Удалить переименованное представление.
CREATE VIEW customers_employees AS
SELECT order_date, required_date, shipped_date, ship_postal_code, company_name, contact_name, phone, last_name, first_name, title
FROM orders
JOIN employees USING(employee_id)
JOIN customers USING(customer_id);

INSERT INTO customers_employees (ship_country, ship_postal_code, contact_name)
VALUES(
	'USA', '123123', 'something'
);

ALTER VIEW customers_employees RENAME TO customers_employees_1;

SELECT * FROM customers_employees_1
ORDER BY ship_country ASC;

-- 3.  Создать представление "активных" (discontinued = 0) продуктов, содержащее все колонки. 
-- Представление должно быть защищено от вставки записей, в которых discontinued = 1.
-- Попробовать сделать вставку записи с полем discontinued = 1 - убедиться, что не проходит.
CREATE VIEW active_products AS
SELECT *
FROM products
WHERE discontinued = 0
WITH LOCAL CHECK OPTION;


CASE WHEN, , COALESCE и NULLIF
-------------------------------------------------------------------------

SELECT title,
       length,
       CASE
           WHEN length> 0
                AND length <= 50 THEN 'Short'
           WHEN length > 50
                AND length <= 120 THEN 'Medium'
           WHEN length> 120 THEN 'Long'
       END duration
FROM film
ORDER BY title;


COALESCE(arg1, arg2) - заменяет Null значения поля из аргумента arg1 на значение arg2

NULLIF(arg1, arg2) - сравнивает значения аргументов и возвращает Null если они равны


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Выполните следующий код (записи необходимы для тестирования корректности выполнения ДЗ):
-- insert into customers(customer_id, contact_name, city, country, company_name)
-- values 
-- ('AAAAA', 'Alfred Mann', NULL, 'USA', 'fake_company'),
-- ('BBBBB', 'Alfred Mann', NULL, 'Austria','fake_company');
-- После этого выполните задание:
-- Вывести имя контакта заказчика, его город и страну, отсортировав по возрастанию по имени контакта и городу,
-- а если город равен NULL, то по имени контакта и стране. Проверить результат, используя заранее вставленные строки.
SELECT contact_name, city, country
FROM customers
ORDER BY contact_name,
(CASE
	WHEN city IS NULL
		THEN country
	ELSE
		city
END);

-- 2. Вывести наименование продукта, цену продукта и столбец со значениями
-- too expensive если цена >= 100
-- average если цена >=50 но < 100
-- low price если цена < 50
SELECT product_name, unit_price, 
	CASE 
	WHEN unit_price >= 100 THEN 'too expensive'
	WHEN unit_price >= 50 AND unit_price < 100 THEN 'average'
	ELSE 'low price'
	END
FROM products;


-- 3. Найти заказчиков, не сделавших ни одного заказа. Вывести имя заказчика и значение 'no orders' если order_id = NULL.
SELECT company_name, COALESCE(order_id::text, 'no orders') FROM customers
LEFT JOIN orders USING(customer_id)
WHERE order_id IS NULL;

-- 4. Вывести ФИО сотрудников и их должности. В случае если должность = Sales Representative вывести вместо неё Sales Stuff.
ELECT last_name || ' ' || first_name, COALESCE(NULLIF(title, 'Sales Representative'), 'Sales Stuff') 
FROM employees;


Functions
-------------------------------------------------------------------------

CREATE FUNCTION func_name([arg1, arg2, ...]) RETURNS data_type AS $$
 - some logic
$$ LANGUAGE sql

-- PL/pgSQL
CREATE FUNCTION func_name([arg1, arg2, ...]) RETURNS data_type AS $$
BEGIN
 - some logic
END;
$$ LANGUAGE plpgsql;

-- декларация переменных
CREATE FUNCTION func_name([arg1, arg2, ...]) RETURNS data_type AS $$
DECLARE
	variable type;
BEGIN
 - some logic
END;
$$ LANGUAGE plpgsql

-- IF/ELSE
IF expression THEN
- some logic
ELSIF expression THEN
- some logic
ELSIF expression THEN
- some logic
ELSE
- some logic
END IF

-- Циклы
WHILE expression
LOOP
	- some logic
END LOOP;

LOOP
	EXIT WHEN expression
	- some logic
END LOOP;

FOR counter IN a..b [BY x]
LOOP
	- some logic
END LOOP;

-- пропустить итерацию
CONTINUE WHEN expression

-- накаливать записи в результирующем наборе
RETURN NEXT expression


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Создайте функцию, которая делает бэкап таблицы customers (копирует все данные в другую таблицу), предварительно стирая таблицу для бэкапа, если такая уже существует 
-- (чтобы в случае многократного запуска таблица для бэкапа перезатиралась).
CREATE FUNCTION backapp_cusomers() RETURNS void AS $$
	DROP TABLE IF EXISTS backapp_cusomers;
	SELECT *
	INTO backapp_cusomers
	FROM customers;
$$ LANGUAGE sql;


-- 2. Создать функцию, которая возвращает средний фрахт (freight) по всем 
CREATE OR REPLACE FUNCTION avg_freight() RETURNS floats AS $$
	SELECT AVG(freight) FROM orders;
$$ LANGUAGE sql;

SELECT * FROM avg_freight();

-- 3. Написать функцию, которая принимает два целочисленных параметра, используемых как нижняя и верхняя границы для генерации случайного числа в пределах этой границы (включая сами граничные значения).
-- Функция random генерирует вещественное число от 0 до 1.
-- Необходимо вычислить разницу между границами и прибавить единицу.
-- На полученное число умножить результат функции random() и прибавить к результату значение нижней границы.
-- Применить функцию floor() к конечному результату, чтобы не "уехать" за границу и получить целое число.
CREATE OR REPLACE FUNCTION random_int(min_int int, max_int int) RETURNS int AS $$
	DECLARE
		random int;
		difference int;
		result int;
	BEGIN
		random := random();
		difference := max_int - min_int + 1;
		result := difference * random + min_int;
		RETURN floor(result);
	END
$$ LANGUAGE plpgsql;

SELECT * FROM random_int(3, 6);

-- 4. Создать функцию, которая возвращает самые низкую и высокую зарплаты среди сотрудников заданного города
CREATE OR REPLACE FUNCTION salaries(check_city char, out min_salary numeric, out max_salary numeric) AS $$		
	BEGIN
		SELECT MAX(salary), MIN(salary) FROM orders
		WHERE city = check_city;
	END
$$ LANGUAGE plpgsql;

SELECT * FROM salaries('Seattle');

-- 5. Создать функцию, которая корректирует зарплату на заданный процент,  но не корректирует зарплату, если её уровень превышает заданный уровень при этом верхний уровень зарплаты по умолчанию равен 70,
-- а процент коррекции равен 15%.
CREATE OR REPLACE FUNCTION correct_salaries(upper_boundary numeric DEFAULT 70, correction numeric DEFAULT 0.15)
RETURNS void AS $$		
		UPDATE employees
		SET salary = salary + salary * 0.15
		WHERE salary <= upper_boundary;
$$ LANGUAGE SQL;

SELECT correct_salaries();

-- 6. Модифицировать функцию, корректирующую зарплату таким образом, чтобы в результате коррекции, она так же выводила бы изменённые записи.
CREATE OR REPLACE FUNCTION correct_salaries(upper_boundary numeric DEFAULT 70, correction numeric DEFAULT 0.15)
RETURNS employees AS $$		
		UPDATE employees
		SET salary = salary + salary * 0.15
		WHERE salary <= upper_boundary
		RETURNING *;
$$ LANGUAGE SQL;

SELECT * FROM correct_salaries();

-- 7. Модифицировать предыдущую функцию так, чтобы она возвращала только колонки last_name, first_name, title, salary
CREATE OR REPLACE FUNCTION correct_salaries(upper_boundary numeric DEFAULT 70, correction numeric DEFAULT 0.15)
RETURNS TABLE(last_name text, first_name text, title text, salary numeric) AS $$		
		UPDATE employees
		SET salary = salary + salary * 0.15
		WHERE salary <= upper_boundary
		RETURNING (last_name, first_name, title, salary);
$$ LANGUAGE SQL;

--  
CREATE OR REPLACE FUNCTION get_orders(delivary_type int) RETURNS setof orders AS $$		
	DECLARE
		max_feight real;
		corrected_freight real;
		avg_freight real;
		avg_general real;
	BEGIN
		max_feight := MAX(freight) FROM orders WHERE ship_via = delivary_type;
		corrected_freight := max_feight - max_feight * 0.3;
		avg_freight := AVG(freight) FROM orders WHERE ship_via = delivary_type;
		avg_general := (corrected_freight + avg_freight) / 2;
		RETURN QUERY
		SELECT * FROM orders WHERE freight < avg_general;
	END
$$ LANGUAGE plpgsql;

SELECT * FROM get_orders(1);

-- 9. Написать функцию, которая принимает:
-- уровень зарплаты, максимальную зарплату (по умолчанию 80) минимальную зарплату (по умолчанию 30), коээфициет роста зарплаты (по умолчанию 20%)
-- Если зарплата выше минимальной, то возвращает false
-- Если зарплата ниже минимальной, то увеличивает зарплату на коэффициент роста и проверяет не станет ли зарплата после повышения превышать максимальную.
-- Если превысит - возвращает false, в противном случае true.
-- Проверить реализацию, передавая следующие параметры
-- (где c - уровень з/п, max - макс. уровень з/п, min - минимальный уровень з/п, r - коэффициент):
-- c = 40, max = 80, min = 30, r = 0.2 - должна вернуть false
-- c = 79, max = 81, min = 80, r = 0.2 - должна вернуть false
-- c = 79, max = 95, min = 80, r = 0.2 - должна вернуть true
CREATE OR REPLACE FUNCTION check_salary(
	currentSalary numeric,
	maximum numeric DEFAULT 80,
	minimum numeric DEFAULT 30,
	r numeric DEFAULT 0.2
) RETURNS boolean AS $$		
	DECLARE
		new_salary numeric;
	BEGIN
		IF currentSalary >= minimum THEN 
			RETURN false;
		END IF;
		IF currentSalary < minimum THEN 
			new_salary = (currentSalary + currentSalary * r);
		END IF;
		IF new_salary > maximum THEN
			RETURN false;
		ELSE
			RETURN true;
		END IF;
	END
$$ LANGUAGE plpgsql;

SELECT check_salary(79, 95, 80);


Обработка ошибок
-------------------------------------------------------------------------

RAISE [level] 'message (%)', arg_name;


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- Имеется следующая функция, которую мы написали в разделе, посвящённом, собственно, функциям:
create or replace function should_increase_salary(
	cur_salary numeric,
	max_salary numeric DEFAULT 80, 
	min_salary numeric DEFAULT 30,
	increase_rate numeric DEFAULT 0.2
	) returns bool AS $$
declare
	new_salary numeric;
begin
	if cur_salary >= max_salary or cur_salary >= min_salary then 		
		return false;
	end if;
	
	if cur_salary < min_salary then
		new_salary = cur_salary + (cur_salary * increase_rate);
	end if;
	
	if new_salary > max_salary then
		return false;
	else
		return true;
	end if;	
end;
$$ language plpgsql;

-- Задание:
-- Модифицировать функцию should_increase_salary разработанную в секции по функциям таким образом, чтобы запретить (выбрасывая исключения) передачу аргументов так, что:
-- минимальный уровень з/п превышает максимальный
-- ни минимальный, ни максимальный уровень з/п не могут быть меньше нуля
-- коэффициент повышения зарплаты не может быть ниже 5%
-- Протестировать реализацию, передавая следующие значения аргументов
-- (с - уровень "проверяемой" зарплаты, r - коэффициент повышения зарплаты):
-- c = 79, max = 10, min = 80, r = 0.2
-- c = 79, max = 10, min = -1, r = 0.2
-- c = 79, max = 10, min = 10, r = 0.04

create or replace function should_increase_salary(
	cur_salary numeric,
	max_salary numeric DEFAULT 80, 
	min_salary numeric DEFAULT 30,
	increase_rate numeric DEFAULT 0.2
	) returns bool AS $$
declare
	new_salary numeric;
begin
	IF min_salary > max_salary THEN
		RAISE EXCEPTION 'min salary should be less than max one %', min_salary, max_salary;
	END IF;

	IF min_salary < 0 OR max_salary < 0 THEN
		RAISE EXCEPTION 'min or max salary should not be less than zero %', min_salary, max_salary;
	END IF;

	IF increase_rate < 0.05 THEN
		RAISE EXCEPTION 'increase rate should not be less than 5% %', increase_rate;
	END IF;

	if cur_salary >= max_salary or cur_salary >= min_salary then 		
		return false;
	end if;
	
	if cur_salary < min_salary then
		new_salary = cur_salary + (cur_salary * increase_rate);
	end if;
	
	if new_salary > max_salary then
		return false;
	else
		return true;
	end if;	
end;
$$ language plpgsql;


Индексы
-------------------------------------------------------------------------

CREATE INDEX index_name ON table_name(column1, column2);


Массивы
-------------------------------------------------------------------------

-- SQL - стандарт
temp int ARRAY
temp int ARRAY[4]

-- Postgres
temp int[]
temp int[4]


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- Создать функцию, которая вычисляет средний фрахт по заданным странам (функция принимает список стран).
CREATE OR REPLACE FUNCTION get_avg_freight_by_countries(VARIADIC countries text[])
RETURNS float8 AS $$
BEGIN
	RETURN AVG(freight)
	FROM orders
	WHERE ship_country = ANY(countries);
END;
$$ language plpgsql;

SELECT get_avg_freight_by_countries('France', 'Brazil');

-- Написать функцию, которая фильтрует телефонные номера по коду оператора.
-- Принимает 3-х значный код мобильного оператора и список телефонных номеров в формате +1(234)5678901 (variadic)
-- Функция возвращает только те номера, код оператора которых соответствует значению соответствующего аргумента.
-- Проверить функцию передав следующие аргументы:
-- 903, +7(903)1901235, +7(926)8567589, +7(903)1532476
-- Попробовать передать аргументы с созданием массива и без.
-- Подсказка: чтобы передать массив в VARIADIC-аргумент, надо перед массивом прописать, собственно, ключевое слово variadic.
CREATE OR REPLACE FUNCTION filter_phones(oper int, VARIADIC numbers text[])
RETURNS setof text AS $$
BEGIN
	FOREACH cur_val IN ARRAY numbers
	LOOP
		CONTINUE WHEN cur_val NOT LIKE CONCAT('__(', oper, ')%');
		RETURN NEXT cur_val;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

SELECT * FROM filter_phones(903, '+7(903)1901235', '+7(926)8567589', '+7(903)1532476');


Пользовательские типы данных
-------------------------------------------------------------------------

-- Домены
CREATE DOMAIN domain_name AS data_type CONSTRAINTS;

-- Составные типы
CREATE TYPE type_name AS (
	field1 type,
	field2 type
);

-- Перечисления
CREATE TYPE type_name AS ENUM('value1', 'value2');


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Переписать функцию, которую мы разработали ранее в одном из ДЗ таким образом, чтобы функция возвращала экземпляр композитного типа. Вот та самая функция:
-- create or replace function get_salary_boundaries_by_city(
-- 	emp_city varchar, out min_salary numeric, out max_salary numeric) 
-- AS 
-- $$
-- 	SELECT MIN(salary) AS min_salary,
-- 	   	   MAX(salary) AS max_salary
--   	FROM employees
-- 	WHERE city = emp_city
-- $$ language sql;


CREATE TYPE salary_bounds AS (
	min_salary numeric,
	max_salary numeric
);

create or replace function get_salary_boundaries_by_city(emp_city varchar)
returns setof salary_bounds AS 
$$
	SELECT MIN(salary) AS min_salary,
	   	   MAX(salary) AS max_salary
  	FROM employees
	WHERE city = emp_city
$$ language sql;

-- 2. Задание состоит из пунктов:
-- Создать перечисление армейских званий США, включающее следующие значения: Private, Corporal, Sergeant
-- Вывести все значения из перечисления.
-- Добавить значение Major после Sergeant в перечисление
-- Создать таблицу личного состава с колонками: person_id, first_name, last_name, person_rank (типа перечисления)
-- Добавить несколько записей, вывести все записи из таблицы

CREATE TYPE army_rank AS ENUM('Private', 'Corporal', 'Sergeant');

SELECT enum_range(null:army_rank);

ALTER TYPE army_rank
ADD VALUE 'Major' AFTER 'Sergeant'; 


CREATE TABLE personnel (
	person_id serial PRIMARY KEY,
	first_name text,
	last_name text,
	person_rank army_rank
);

INSERT INTO personnel (first_name, last_name, person_rank)
VALUES('Ivan', 'Ivanov', 'Private');


Продвинуты группировки
-------------------------------------------------------------------------


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- Вывести сумму продаж (цена * кол-во) по каждому сотруднику с подсчётом полного итога (полной суммы по всем сотрудникам) отсортировав по сумме продаж (по убыванию).
SELECT employee_id, SUN(unit_price*quantity)
FROM orders
LEFT JOIN order_details USING(order_id)
GROUP BY ROLLUP(employee_id)
ORDER BY SUN(unit_price*quantity) DESC;

-- Вывести отчёт показывающий сумму продаж по сотрудникам и странам отгрузки с подытогами по сотрудникам и общим итогом.
SELECT employee_id, ship_country SUN(unit_price*quantity)
FROM orders
LEFT JOIN order_details USING(order_id)
GROUP BY ROLLUP(employee_id, ship_country)
ORDER BY employee_id, SUN(unit_price*quantity) DESC;

-- Вывести отчёт показывающий сумму продаж по сотрудникам, странам отгрузки, сотрудникам и странам отгрузки с подытогами по сотрудникам и общим итогом.
SELECT employee_id, ship_country SUN(unit_price*quantity)
FROM orders
LEFT JOIN order_details USING(order_id)
GROUP BY CUBE(employee_id, ship_country)
ORDER BY employee_id, SUN(unit_price*quantity) DESC;


CTE - Common Table Expression - для создания временных таблиц
-------------------------------------------------------------------------

WITH name AS (
	SELECT clause
)
SELECT 'using WITH part';


Оконные функции (Window Functions)
-------------------------------------------------------------------------

function OVER (expression)

function OVER ([PARTITION BY expression], [ORDER BY expression])


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- Вывести отчёт показывающий по сотрудникам суммы продаж SUM(unit_price*quantity), и сопоставляющий их со средним значением суммы продаж по сотрудникам (AVG по SUM(unit_price*quantity)) 
-- сортированный по сумме продаж по убыванию.
SELECT employee_id, total_sum, AVG(total_sum)
FROM (
	SELECT employee_id, SUM(unit_price * quantity) OVER (PARTITION BY employee_id) AS total_sum
	FROM orders
	LEFT JOIN order_details USING(order_id)
)
ORDER BY total_sum DESC;

-- Вывести ранг сотрудников по их зарплате, без пропусков. Также вывести имя, фамилию и должность.	
SELECT first_name, last_name, title, DENSE_RANK() OVER(ORDER_BY salary)
FROM employees;


Транзакции
-------------------------------------------------------------------------

TCL - Transaction Control Language

START TRANSACTION / BEGIN [TRANSACTION]

END [TRANSACTION] / COMMIT

BEGIN / COMMIT - SQL standard

START TRANSACTION / END [TRANSACTION] - pgSQL extension

-- example:
BEGIN;
statement1;
statement2;
statement3;
COMMIT;


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. В рамках транзакции с уровнем изоляции Repeatable Read выполнить следующие операции:
-- - заархивировать (SELECT INTO или CREATE TABLE AS) заказчиков, которые сделали покупок менее чем на 2000 у.е.
-- - удалить из таблицы заказчиков всех заказчиков, которые были предварительно заархивированы (подсказка: для этого придётся удалить данные из связанных таблиц)
BEGIN TRANSACTION ISOLATION LEVEL REPEATABLE READ;

CREATE TABLE poor_customers AS
	SELECT customer_id, company_name, SUM(unit_price * quantity) AS total
	FROM orders
	JOIN order_details USING(order_id)
	JOIN customers USING(customer_id)
	GROUP BY company_name, customer_id
	HAVING SUM(unit_price * price) < 2000
	ORDER BY SUM(unit_price * quantity) DESC;

	DELETE FROM order_details

	WHERE order_id IN (SELECT order_id
						FROM orders
						WHERE customer_id IN
							(SELECT customer_id FROM archive_poor_customers)
						);

	DELETE FROM orders
	WHERE customer_id IN (SELECT customer_id FROM archive_poor_customers);

	DELETE FROM customers
	WHERE customer_id IN (SELECT customer_id FROM archive_poor_customers);

COMMIT;

-- 2. В рамках транзакции выполнить следующие операции:
-- - заархивировать все продукты, снятые с продажи (см. колонку discontinued)
-- - поставить savepoint после архивации
-- - удалить из таблицы продуктов все продукты, которые были заархивированы
-- - откатиться к savepoint
-- - закоммитить тразнакцию
BEGIN;

CREATE TABLE discontinued_products AS
	SELECT * FROM products WHERE discontinued = 1;

SAVEPOINT discontinued;

DELETE FROM order_details
WHERE product_id IN (SELECT product_id FROM discontinued_products);

DELETE FROM products
WHERE discontinued = 1;

ROLLBACK TO discontinued;

COMMIT;


Триггеры
-------------------------------------------------------------------------

-- Построчные триггеры:

-- Создание триггера:
CREATE TRIGGER trigger_name() condition ON table_name
FOR EACH ROW EXECUTE PROCEDURE func_name();

-- condition:
[BEFORE, AFTER] [INSERT, UPDATE, DELETE]

-- Например:
BEFORE INSERT
AFTER UPDATE
BEFORE INSERT OR UPDATE

-- Вид функции, привязываемой треггером:
CREATE FUNCTION func_name() RETURNS trigger AS $$
BEGIN
-- -------
END;
$$ LANGUAGE plpgsql;

-- Триггеры на утверждения:

-- создание триггера:
CREATE TRIGGER trigger_name() condition ON table_name
REFERENCING [NEW, OLD] TABLE AS ref_table_name
FOR EACH STATEMENT EXECUTE PROCEDURE func_name();

-- Удаление триггера:
DROP TRIGGER IF EXISTS trigger_name ON table_name

-- Переименование триггера:
ALTER TRIGGER trigger_name ON table_name
RENAME TO new_trigger_name;

-- Отключение триггера:
ALTER TABLE table_name
DISABLE TRIGGER trigger_name;

-- Отключения всех триггеров на таблице:
ALTER TABLE table_name
DISABLE TRIGGER ALL;


Домашнее задание (Практика):
-------------------------------------------------------------------------

-- 1. Автоматизировать логирование времени последнего изменения в таблице products. Добавить в products соответствующую колонку и реализовать построчный триггер.
ALTER TABLE products
ADD COLUMN last_updated 'timestamp';

CREATE OR REPLACE FUNCTION track_products_changes() RETURNS trigger AS $$
BEGIN
	NEW.last_updated = now();
	RETURN NEW;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS products_timestamp ON products;
CREATE TRIGGER products_timestamp BEFORE INSERT OR UPDATE ON products
FOR EACH ROW EXECUTE PROCEDURE track_products_changes();

SELECT last_updated, * FROM products
WHERE product_id = 2;

UPDATE products
SET unit_price = 19.05
WHERE product_id = 2;

ALTER TABLE products
DISABLE TRIGGER audit_products_update;

-- 2. Автоматизировать аудит операций в таблице order_details. Создайте отдельную таблицу для аудита, добавьте туда колонки для хранения наименования операций,
-- имени пользователя и временного штампа. Реализуйте триггеры на утверждения.
DROP TABLE IF EXISTS order_details_audit;
CREATE TABLE order_details_audit (
	op char(1) NOT NULL,
	user_changed text NOT NULL,
	time_stamp timestamp NOT NULL,

	order_id smallint NOT NULL,
	product_id smallint NOT NULL,
	unit_price real NOT NULL,
	quantity smallint NOT NULL,
	discount real
);

CREATE OR REPLACE FUNCTION build_audit_order_details() RETURNS trigger AS $$
BEGIN
	IF TG_OP = 'INSERT' THEN
		INSERT INTO order_details_audit
		SELECT 'I' session_user, now(), nt.* FROM new_table nt;
	ELSEIF TG_OP = 'UPDATE' THEN
		INSERT INTO order_details_audit
		SELECT 'U' session_user, now(), nt.* FROM new_table nt;
	ELSEIF TG_OP = 'DELETE' THEN
		INSERT INTO order_details_audit
		SELECT 'D' session_user, now(), ot.* FROM old_table ot;
	END IF;
	RETURN NULL;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS audit_order_details_insert ON order_details;
CREATE TRIGGER audit_order_details_insert AFTER INSERT ON order_details
REFERENCING NEW TABLE AS new_table
FOR EACH STATEMENT EXECUTE PROCEDURE build_audit_order_details();

DROP TRIGGER IF EXISTS audit_order_details_update ON order_details;
CREATE TRIGGER audit_order_details_update AFTER UPDATE ON order_details
REFERENCING NEW TABLE AS new_table
FOR EACH STATEMENT EXECUTE PROCEDURE build_audit_order_details();

DROP TRIGGER IF EXISTS audit_order_details_delete ON order_details;
CREATE TRIGGER audit_order_details_delete AFTER DELETE ON order_details
REFERENCING NEW TABLE AS new_table
FOR EACH STATEMENT EXECUTE PROCEDURE build_audit_order_details();