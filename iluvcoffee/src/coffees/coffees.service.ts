import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { CreateCoffeeDto } from './dto/create-coffee.dto';
import { PaginationQueryDto } from './dto/pagination-query.dto';
import { UpdateCoffeeDto } from './dto/update-coffee.dto';
import { Coffee } from './entities/coffee.entity';
import { Flavor } from './entities/flavor.entity';
import { Event } from '../events/entities/event.entity';
import { COFFEE_BRANDS } from './coffees.constants';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class CoffeesService {

    constructor(
        @InjectRepository(Coffee)
        private readonly coffeeRepository: Repository<Coffee>,

        @InjectRepository(Flavor)
        private readonly flavorRepository: Repository<Flavor>,

        private readonly connection: Connection,

        @Inject(COFFEE_BRANDS) coffeeBrands: string[],

        private readonly configService: ConfigService,

        @InjectModel(Coffee.name) private readonly coffeeModel: Model<Coffee>
    ) {}

    async findAll(paginationQuery: PaginationQueryDto): Promise<Coffee[]> {
        const { offset, limit } = paginationQuery;
        return this.coffeeRepository.find({
            relations: ['flavors'],
            skip: offset,
            take: limit,
        });

        // return this.coffeeModel.find().skip(offset).limit(limit).exec();
    }

    async findOne(id: string): Promise<Coffee> {
        const coffee = await this.coffeeRepository.findOne({
            where: {id: parseInt(id)},
            relations: ['flavors']
        });
        // const coffee = await this.coffeeRepository.findOne({ _id: id }).exec();
        if (!coffee) {
            throw new NotFoundException(`Coffee #${id} not found`);
        }
        return coffee;
    }

    async create(createCoffeeDto: CreateCoffeeDto): Promise<Coffee> {
        const flavors = await Promise.all(
            createCoffeeDto.flavors.map(name => this.preloadFlavorByName(name))
        );
        const coffee = await this.coffeeRepository.create({
            ...createCoffeeDto,
            flavors,
        });
        return this.coffeeRepository.save(coffee);

        // const coffee = new this.coffeeModel(createCoffeeDto);
        // return coffee.save();
    }

    async update(id: string, updateCoffeeDto: UpdateCoffeeDto): Promise<Coffee> {
        const flavors = await Promise.all(
            updateCoffeeDto.flavors.map(name => this.preloadFlavorByName(name))
        );
        const coffee = await this.coffeeRepository.preload({
            id: parseInt(id),
            ...updateCoffeeDto,
            flavors,
        });

        // const coffee = await this.coffeeModel
        //     .findOneAndUpdate({ _id: id }, { $set: updateCoffeeDto }, { new: true })
        //     .exec();

        if (!coffee) {
            throw new NotFoundException(`Coffee #${id} not found`);
        }
        return this.coffeeRepository.save(coffee);
    }

    async remove(id: string): Promise<Coffee> {
        const coffee = await this.coffeeRepository.findOne({where: {id: parseInt(id)} });
        return this.coffeeRepository.remove(coffee);

        // const coffee = await this.findOne(id);
        // return coffee.remove();
    }

    async recommendCoffee(coffee: Coffee) {
        const queryRunner = await this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            coffee.recommendations++;

            const recommendEvent = new Event();
            recommendEvent.name = 'recommend_coffee';
            recommendEvent.type = 'coffee';
            recommendEvent.payload = { coffeeId: coffee.id };

            await queryRunner.manager.save(coffee);
            await queryRunner.manager.save(recommendEvent);

            await queryRunner.commitTransaction();
        } catch (err) {
            await queryRunner.rollbackTransaction();
        } finally {
            await queryRunner.release();
        }
    }

    private async preloadFlavorByName(name: string) {
        const existingFlavor = await this.flavorRepository.findOne({ where: { name } });
        if (existingFlavor) {
            return existingFlavor;
        }
        return this.flavorRepository.create({name});
    }
}
