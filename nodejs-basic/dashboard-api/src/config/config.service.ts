import { config, DotenvConfigOutput, DotenvParseOutput } from 'dotenv';
import { inject, injectable } from 'inversify';
import { ILogger } from '../logger/logger.interface';
import { TYPES } from '../types';
import { IConfigService } from './config.service.interface';

@injectable()
export class ConfigService implements IConfigService {
    private config: DotenvParseOutput | undefined;

    constructor(
        @inject(TYPES.ILogger) private readonly logger: ILogger
    ) {
        const result: DotenvConfigOutput = config();
        if (result.error) {
            this.logger.error('There is no .env file');
        } else {
            this.config = result.parsed as DotenvParseOutput;
        }
    }

    get(key: string): string {
        if (this.config === undefined) {
            throw new Error('Config was not set');
        }
        return this.config[key];
    }
}