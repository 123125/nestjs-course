import { Request, Response, NextFunction } from 'express';
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { LoggerService } from '../logger/logger.service';
import { TYPES } from '../types';
import { HTTPError } from './http-error.class';

@injectable()
export class ExceptionFilter implements ExceptionFilter {
	constructor(@inject(TYPES.ILogger) private readonly logger: LoggerService) {}
	catch(err: Error | HTTPError, req: Request, res: Response, next: NextFunction): void {
		if (err instanceof HTTPError) {
			this.logger.error(`[${err.context}] Error: ${err.statusCode}: ${err.message}`);
		} else {
			this.logger.error(`${err.message}`);
			res.status(500).send({ err: err.message });
		}
	}
}
