import { NextFunction, Response, Request } from 'express';
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { BaseController } from '../common/base.controller';
import { IControllerRoute } from '../common/route.interface';
import { TYPES } from '../types';
import { ILogger } from '../logger/logger.interface';
import { IUserController } from './users.interface';
import { UserRegisterDto } from './dto/user-register.dto';
import { UserLoginDto } from './dto/user-login.dto';
import { HTTPError } from '../errors/http-error.class';
import { ValidateMiddleware } from '../common/validate.middleware';
import { sign } from 'jsonwebtoken';
import { IUserService } from './user.service.interface';
import { IConfigService } from '../config/config.service.interface';
import { AuthGuard } from '../common/auth.guard';

@injectable()
export class UserController extends BaseController implements IUserController {
	private routes: IControllerRoute[] = [
		{
			path: '/login',
			method: 'post',
			func: this.login,
			middleware: [new ValidateMiddleware(UserRegisterDto)]
		},
		{
			path: '/register',
			method: 'post',
			func: this.register,
			middleware: [new ValidateMiddleware(UserLoginDto)]
		},
		{
			path: '/info',
			method: 'get',
			func: this.info,
			middleware: [new AuthGuard()]
		},
	];

	constructor(
        @inject(TYPES.ILogger) private readonly loggerService: ILogger,
        @inject(TYPES.UserService) private readonly userService: IUserService,
        @inject(TYPES.ConfigService) private readonly configService: IConfigService,
    ) {
		super(loggerService);

		this.bindRoutes(this.routes);
	}

	async login(
		{ body }: Request<{}, {}, UserLoginDto>,
		res: Response,
		next: NextFunction
	): Promise<void> {
		const user = await this.userService.validateUser(body);
		if (user === null || body.email === undefined) {
            return next(new HTTPError(401, 'User has not been registered'))
        }
		const jwt = await this.signJWT(body.email, this.configService.get('SECRET'));
		this.ok(res, { jwt });
	}

	async register(
        { body }: Request<{}, {}, UserRegisterDto>,
        res: Response,
        next: NextFunction
    ): Promise<void> {
        const newUser = await this.userService.createUser(body);
        if (newUser === null) {
            return next(new HTTPError(422, 'User has already existed'))
        }
		this.ok(res, newUser);
	}

	async info(
		{ user }: Request<{}, {}, string>,
        res: Response,
        next: NextFunction
    ): Promise<void> {
		const userInfo = await this.userService.getUserInfo(user); 
		this.ok(res, { email: userInfo?.email, id: userInfo?.id });
	}

	private signJWT(email: string, secret: string): Promise<string> {
		return new Promise<string>((resolve, reject) => {
			sign(
				{
					email,
					iat: Math.floor(Date.now() / 1000)
				},
				secret,
				{
					algorithm: 'HS256',
				},
				(err, token) => {
					if (err) {
						reject(err);
					}
					resolve(token as string);
				}
			)
		})
	}
}
