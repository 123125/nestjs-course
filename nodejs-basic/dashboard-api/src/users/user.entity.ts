import { hash, compare } from 'bcryptjs';

export class User {
    private _password: string | undefined;

    constructor(
        private readonly _email: string,
        private readonly _name: string,
    ) {}

    get email(): string {
        return this._email;
    }

    get name(): string {
        return this._name;
    }

    get password(): string | undefined {
        return this._password;
    }

    public async setPassword(password: string, salt: number): Promise<void> {
        this._password = await hash(password, salt);
    }

    public async validatePassword(userPassword: string, dbHash: string): Promise<boolean> {
        return compare(userPassword, dbHash);
    }
}