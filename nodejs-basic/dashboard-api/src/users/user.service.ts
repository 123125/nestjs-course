import { injectable, inject } from 'inversify';
import { UserLoginDto } from './dto/user-login.dto';
import { UserRegisterDto } from './dto/user-register.dto';
import { User } from './user.entity';
import { IUserService } from './user.service.interface';
import { IConfigService } from '../config/config.service.interface';
import { TYPES } from '../types';
import { IUserRepository } from './user.repository.interface';
import { UserModel } from '@prisma/client';

@injectable()
export class UserService implements IUserService {
    constructor(
        @inject(TYPES.ConfigService) private readonly configService: IConfigService,
        @inject(TYPES.UserRepository) private readonly userRepository: IUserRepository,
    ) {}

    async createUser({ email, name, password }: UserRegisterDto): Promise<UserModel | null> {
        if ( email !== undefined && name !== undefined && password !== undefined) {
            const newUser = new User(email, name);
            const salt = this.configService.get('SALT');
            await newUser.setPassword(password, Number(salt));
            const existedUser = await this.userRepository.find(email);
            if (existedUser) {
                return null
            }
            return this.userRepository.create(newUser);
        }
        return null;
    }

    async validateUser({ email, password }: UserLoginDto): Promise<boolean> {
        if (email === undefined || password === undefined) return false;
        const user = await this.userRepository.find(email);
        if (user === null) return false;
        return new User(user.email, user.name).validatePassword(password, user.password);
    }

    async getUserInfo(email: string): Promise<UserModel | null> {
        return this.userRepository.find(email);
    }
}