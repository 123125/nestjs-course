import { IsEmail, IsString } from 'class-validator';

export class UserRegisterDto {
    @IsEmail({}, { message: 'Wrong email' })
    email: string | undefined;

    @IsString({ message: 'Wrong password' })
    password: string | undefined;

    @IsString({ message: 'Wrong name' })
    name: string | undefined;
}