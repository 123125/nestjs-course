import { IsEmail, IsString } from 'class-validator';

export class UserLoginDto {
    @IsEmail({}, { message: 'Wrong email'})
    email: string | undefined;

    @IsString({ message: 'Wrong password' })
    password: string | undefined;
}