const { Worker } = require('worker_threads');
const { fork } = require('child_process');
const { performance, PerformanceObserver } = require('perf_hooks');

const performanceObserver = new PerformanceObserver((items, observer) => {
	items.getEntries().forEach((entry) => {
		console.log(`${entry.name}: ${entry.duration}`);
		console.log(`${JSON.stringify(entry)}`);
	});
});

performanceObserver.observe({ entryTypes: ['measure'] });

const workerFunction = (array) => {
	return new Promise((resolve, reject) => {
		performance.mark('workerFunction start');
		const worker = new Worker('./worker.js', {
			workerData: {
				array
			}
		});

		worker.on('message', (msg) => {
			performance.mark('workerFunction end');
			performance.measure('workerFunction', 'workerFunction start', 'workerFunction end');
			resolve(msg);
		});
	});
};

const forkFunction = (array) => {
	return new Promise((resolve, reject) => {
		performance.mark('forkFunction start');
		const forkProcess = fork('./fork.js');
		forkProcess.send({ array });

		forkProcess.on('message', (msg) => {
			performance.mark('forkFunction end');
			performance.measure('forkFunction', 'forkFunction start', 'forkFunction end');
			resolve(msg);
		});
	});
};

const main = async () => {
	try {
		await workerFunction([25, 20, 19, 48, 30, 50]);
		await forkFunction([25, 20, 19, 48, 30, 50]);
	} catch (e) {
		console.error(e.message);
	}
};

main();