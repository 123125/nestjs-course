import { getArgs } from './helpers/args';
import { getWeather } from './services/api.service';
import { printError, printHelp, printSuccess, printWeather } from './services/log.service';
import { saveKeyValue, getKeyValue, TOKEN_DICTIONARY } from './services/storage.service';

const saveToken = async (token) => {
    if (!token.length) {
        printError('Token isn\'t sent');
        return;
    }
    try {
        await saveKeyValue(TOKEN_DICTIONARY.token, token);
        printSuccess('Token saved');
    } catch (error) {
        printError(error.message);
    }
};

const saveCity = async (city) => {
    if (!token.length) {
        printError('Token isn\'t sent');
        return;
    }
    try {
        await saveKeyValue(TOKEN_DICTIONARY.city, city);
        printSuccess('City saved');
    } catch (error) {
        printError(error.message);
    }
}

const getForCast = async () => {
    try {
        const city = process.env.city ?? await getKeyValue('city');
        const weather = await getWeather(city);
        printWeather(weather, getIcon(weather.weather[0].icon));
    } catch (error) {
        const status = error.response.status;
        if (status === 404) {
            printError('Wrong city');
        } else if (status === 401) {
            printError('Token isn\'t set');
        } else {
            printError(e.message);
        }
    }
}

const initCLI = () => {
    const args = getArgs(process.argv);
    if (args.h) {
        return printHelp();
    }
    if (args.s) {
        return saveCity(args.s);
    }
    if (args.t) {
        return saveToken(args.t);
    }
    return getForCast();
};

initCLI();